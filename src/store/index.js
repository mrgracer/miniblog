import { createStore } from "vuex";

export default createStore({
  state: {
    posts: JSON.parse(localStorage.getItem("posts")) || [],
    comments: JSON.parse(localStorage.getItem("comments")) || []
  },
  mutations: {
    newPost(state, data) {
      data.id = Date.now();
      state.posts.push(data);
    },
    updatePost(state, data) {
      const index = state.posts.findIndex(post => post.id == data.id);
      state.posts[index] = data;
    },
    deletePost(state, postId) {
      const index = state.posts.findIndex(post => post.id == postId);
      if (index >= 0) {
        state.posts.splice(index, 1);
      }
    },
    newComment(state, data) {
      data.id = Date.now();
      state.comments.push(data);
    },
    deleteComment(state, commId) {
      state.comments = state.comments.filter(
        comment => comment.id != commId && comment.answerTo != commId
      );
    },
    deletePostComments(state, postId) {
      state.comments = state.comments.filter(
        comment => comment.postId != postId
      );
    }
  },
  getters: {
    post: state => postId => {
      return state.posts.find(post => post.id == postId);
    },
    postComments: state => postId => {
      return state.comments.filter(
        comment => comment.postId == postId && !comment.answerTo
      );
    },
    commentAnswers: state => commId => {
      return state.comments.filter(comment => comment.answerTo == commId);
    }
  },
  actions: {
    newPost(ctx, data) {
      ctx.commit("newPost", data);
      ctx.dispatch("savePostsState");
    },
    updatePost(ctx, data) {
      ctx.commit("updatePost", data);
      ctx.dispatch("savePostsState");
    },
    deletePost(ctx, postId) {
      ctx.commit("deletePost", postId);
      ctx.commit("deletePostComments", postId);
      ctx.dispatch("savePostsState");
      ctx.dispatch("saveCommentsState");
    },
    newComment(ctx, data) {
      ctx.commit("newComment", data);
      ctx.dispatch("saveCommentsState");
    },
    deleteComment(ctx, commId) {
      const commsToDelete = ctx.state.comments.filter(
        comment => comment.id == commId || comment.answerTo == commId
      );

      ctx.commit("deleteComment", commId);

      commsToDelete.forEach(comment => {
        ctx.dispatch("deleteComment", comment.id);
      });
      ctx.dispatch("saveCommentsState");
    },
    savePostsState(ctx) {
      localStorage.setItem("posts", JSON.stringify(ctx.state.posts));
    },
    saveCommentsState(ctx) {
      localStorage.setItem("comments", JSON.stringify(ctx.state.comments));
    }
  },
  modules: {}
});

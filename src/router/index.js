import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Create from '../views/Create.vue'
import Post from '../views/Post.vue'
import Edit from '../views/Edit.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Главная',
    },
  },
  {
    path: '/create',
    name: 'Create',
    component: Create,
    meta: {
      title: 'Создание поста',
    },
  },
  {
    path: '/post/:id',
    name: 'Post',
    component: Post,
    props: true,
    meta: {
      title: 'Просмотр поста',
    },
  },
  {
    path: '/edit/:id',
    name: 'Edit',
    component: Edit,
    props: true,
    meta: {
      title: 'Редактирование',
    },
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to) => {
  if(to.meta.title) {
    document.title = to.meta.title;
  }
})

export default router
